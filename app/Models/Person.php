<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    public function viloyat()
    {
        return $this->belongsTo(viloyat::class, 'viloyat_id', 'id');
    }

}
