<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $car_name = ['BMW', 'AUDI', 'Nexia', 'Matiz', 'Mersedes'];
        $color = ['black', 'red', 'white','yellow','blue'];
        $price = [45000, 36000, 12000, 2000, 28000];
        $year = [1995, 1998, 2018, 2010, 1967];
        for ($i=0; $i<count($car_name); $i++)
        {
            DB::table('cars')->insert([
                'car_name'=>$car_name[$i],
                'color'=>$color[$i],
                'price'=>$price[$i],
                'year'=>$year[$i]
            ]);
        }
    }
}
