<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->unique()->name,
            'address'=>$this->faker->address,
            'age'=>$this->faker->numberBetween(1,99)
        ];
    }
}
