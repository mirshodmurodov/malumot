<?php

namespace App\Http\Controllers;

use App\Models\viloyat;
use App\Http\Requests\StoreviloyatRequest;
use App\Http\Requests\UpdateviloyatRequest;

class ViloyatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreviloyatRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreviloyatRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\viloyat  $viloyat
     * @return \Illuminate\Http\Response
     */
    public function show(viloyat $viloyat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\viloyat  $viloyat
     * @return \Illuminate\Http\Response
     */
    public function edit(viloyat $viloyat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateviloyatRequest  $request
     * @param  \App\Models\viloyat  $viloyat
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateviloyatRequest $request, viloyat $viloyat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\viloyat  $viloyat
     * @return \Illuminate\Http\Response
     */
    public function destroy(viloyat $viloyat)
    {
        //
    }
}
